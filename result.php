<html>

<head>
	<meta charset="utf-8">
</head>

<body>
	<?php
	$mode = $_POST["mode"];
	$message = strtolower(urlencode($_POST["message"]));
	$cipher = explode(',', strtolower(str_replace(array('[', ']'), '', $_POST["cipher"])));

	if ($mode == "" || $message == "" || $cipher == "") {	// check if input is empty or not, if yes redirect
		header('Location: ' . "index.html");
	}

	$product = "";
	$original = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
	
	$chars = str_split($message);
	if ($mode == "Encryption") {
		foreach ($chars as $char) {
			for ($i = 0; $i < count($original); $i++) {
				if ($char == $original[$i]){
					$product = $product . $cipher[$i];
				}
			}
		}
	} else {
		foreach ($chars as $char) {
			for ($i = 0; $i < count($cipher); $i++) {
				if ($char == $cipher[$i]){
					$product = $product . $original[$i];
				}
			}
		}
	}
	?>

	<h4><?php echo $mode ?> Result</h4>
	<textarea name="text" rows="10"><?php echo $product ?></textarea>

</body>

</html>